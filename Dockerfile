FROM openjdk:8
WORKDIR /root/ikf-sae
COPY ./target/ikeyforge-sae-service-1.0-SNAPSHOT.jar /root/ikf-sae
ENTRYPOINT ["java","-jar","/root/ikf-sae/ikeyforge-sae-service-1.0-SNAPSHOT.jar"]
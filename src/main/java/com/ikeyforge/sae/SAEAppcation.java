package com.ikeyforge.sae;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@NacosPropertySource(dataId = "ikeyforge-sae-service.yml", autoRefreshed = true)
public class SAEAppcation {
    public static void main(String[] args) {
        SpringApplication.run(SAEAppcation.class, args);
    }
}

package com.ikeyforge.sae.controller;

import com.ikeyforge.sae.entity.WorkShopDb;
import com.ikeyforge.sae.service.SAEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SAEController {

    @Autowired
    private SAEService saeService;

    @GetMapping("/testNginx")
    public String testNginx(){
        return saeService.testNginx();
    }

    @GetMapping("/checkHealth")
    public String checkHealth() {
        return saeService.checkHealth();
    }

    @GetMapping("/mockHighRt")
    public String mockHighRt() {
        return saeService.mockHighRt();
    }

    @GetMapping("/mockException")
    public void mockException() throws Exception{
        saeService.mockException();
    }

    @PostMapping("/workShopDB")
    public WorkShopDb saveWorkShopDB(@RequestBody WorkShopDb workShopDB){
        return saeService.saveWorkShopDB(workShopDB);
    }
}

package com.ikeyforge.sae.service;

import com.ikeyforge.sae.entity.WorkShopDb;

public interface SAEService {
    String testNginx();
    String checkHealth();
    String mockHighRt();
    void mockException() throws Exception;
    WorkShopDb saveWorkShopDB(WorkShopDb workShopDB);
}

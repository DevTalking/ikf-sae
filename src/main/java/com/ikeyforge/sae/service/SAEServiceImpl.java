package com.ikeyforge.sae.service;

import com.ikeyforge.sae.entity.WorkShopDb;
import com.ikeyforge.sae.repository.WorkShopDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SAEServiceImpl implements SAEService{

    @Autowired
    private WorkShopDBRepository workShopDBRepository;

    @Override
    public String testNginx() {
        return "This is Nginx test!";
    }

    @Override
    public String checkHealth() {
        return "success!!!!";
    }

    @Override
    public String mockHighRt() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }

    @Override
    public void mockException() throws Exception {
        throw new Exception("this is a exception");
    }

    @Override
    public WorkShopDb saveWorkShopDB(WorkShopDb workShopDB) {
        return workShopDBRepository.save(workShopDB);
    }
}

package com.ikeyforge.sae.repository;

import com.ikeyforge.sae.entity.WorkShopDb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WorkShopDBRepository extends JpaRepository<WorkShopDb, Long>, PagingAndSortingRepository<WorkShopDb, Long>, JpaSpecificationExecutor<WorkShopDb> {
}
